# Todo a icecast

Transmití imágenes, audio y vídeos en un canal de televisión o radio [icecast](http://icecast.org/). La idea es hacer un cliente super 
simple de icecast que sea capaz de transmitir en vivo audio y video, mezclar multiples pistas, crear tuberias entre difrentes 
programas.

## Instalación

~~~
cd /tmp
wget https://raw.github.com/b4zz4/todo2icecast/master/todo2icecast
bash todo2icecast -u
~~~

## Producir vídeos o audios

### Manejo de pistas

~~~
todo2icecast -a imagen.png 
~~~
> Generar la `pista A` con una imagen

~~~
todo2icecast -a video.ogv
~~~
> Generar la `pista A` con un vídeo

~~~
todo2icecast -a audio.ogg
~~~
> Generar la `pista A` con un archivo de audio

~~~
todo2icecast -a Música/
~~~
> Generar la `pista A` a partir de un directorio

~~~
todo2icecast -a c
~~~
> Generar la `pista A` con la webcam y el audio de la pc

~~~
todo2icecast -a m
~~~
> Generar la `pista A` usando el audio de la PC

~~~
todo2icecast -a e
~~~
> Generar la `pista A` con el escritorio

~~~
echo "hola mundo" | todo2icecast -a t 
~~~
> Generar la `pista A` usando texto a voz

~~~
todo2icecast -a z /srv/tftp/
~~~
> **Zombie:** espera cambios en un directorio y los transmite. Especialmente pensado para [tftp](https://es.wikipedia.org/wiki/Trivial_File_Transfer_Protocol), ftp, ssh, etc

~~~
todo2icecast -a rtmp://servidor/transmision
~~~
> Retransmisión de un video transmitido de RTMP. Requiere `rtmpdump` 

## Transmitir

~~~
todo2icecast -o "giss.tv 8000 sf6vh /mashku.ogg -d "descripcion" [-g genero ] [-n nombre ] [-u URL ] "
~~~
> Iniciar el demonio de icecast

## Reproducir
	
~~~ 
todo2icecast -r
~~~
> Reproducir video generado

## Ejemplos complejos

### Radio al instante

~~~
vlc 2> /dev/null &
sleep 1m # tiempo de gracia para buscar temas
rm /tmp/A.mpg
mkfifo /tmp/A.mpg
todo2icecast -a m  2> /dev/null &
sleep 20 # tiempo para cache
todo2icecast -o "127.0.0.1 8000 hackme /audio.ogg" # transmitimos a nuestro icecast
~~~

En este ejemplo usamos [VLC](https://videolan.org/vlc/) para reproducir audio y lo grabamos en la `pista A` luego esta es transmitida a nuestro icecast local. 
Lo mismo se puede hacer para un [Mumble](http://mumble.sourceforge.net/), [Mplayer](http://mplayerhq.hu/), [Linphone](http://www.linphone.org/), etc.
A la vez podemos dejar abierto el micrófono y enviar nuestra vos en vivo.

**Nota:** Si no tenemos punto de montaje de icecast podemos conseguir uno en [giss.tv](http://giss.tv).

### Mezclador

Aun asi pueden hacerse cortes en vivo, cortando la generación de un track y sumando uno nuevo.
El parametro zombie tal vez sea la más apropiada, se queda escuchando un directorio y genera la transmisión a partir de él.
Por ahora las multiples pistas no son estables (mirar rama `mezclador`). 

## Lista de quehaceres

- Que funcione la `pista B` otra pista para mezclar con la principal
  - Usar melt para la mezcla.
- Generar las pistas de modo mas ligero, para mejorar la mezcla en tiempo real.
- Generador de placas del canal, o cortinas para sostener la transmisiones que posiblemente puedan cortarse
  - O tan solo pista en "blanco"
- Servir archivos vía http usando mjpeg y ogg
- Lista a partir de un txt
  - Control remoto con IRC
